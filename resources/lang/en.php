<?php

return [
    'decimal_point' => '.',
    'thousands_separator' => ',',
];
