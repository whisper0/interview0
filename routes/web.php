<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test1',[\App\Http\Controllers\FirstTaskController::class, 'firstTask']);

Route::get('/test2',[\App\Http\Controllers\SecondTaskController::class, 'goToGateway']);
