<?php

namespace App\Providers;

use App\Drivers\Payment\ChizPaymentDriver;
use App\Drivers\Payment\Contracts\PaymentDriverInterface;
use App\Drivers\Payment\FolanPaymentDriver;
use App\Drivers\Payment\ShaparakPaymentDriver;
use App\Services\Payment\Contracts\PaymentServiceInterface;
use App\Services\Payment\PaymentService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentServiceInterface::class, PaymentService::class);

        // Get the user status from the input (you can replace this with your actual logic)
        $gateWay = 'shaparak'; // For example, 'active' or 'inactive'

        // Use conditional binding to determine which implementation to use
        if ($gateWay === 'shaparak') {
            $this->app->bind(PaymentDriverInterface::class, ShaparakPaymentDriver::class);
        } else if($gateWay === 'chiz') {
            $this->app->bind(PaymentDriverInterface::class, ChizPaymentDriver::class);
        } else if($gateWay === 'folan') {
            $this->app->bind(PaymentDriverInterface::class, FolanPaymentDriver::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
