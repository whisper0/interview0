<?php

namespace App\Drivers\Payment\Contracts;

use App\Entities\Payment;

interface PaymentDriverInterface
{
    /**
     * @param Payment $payment
     * @return void
     */
    public function pay(Payment $payment) : void;

    /**
     * @param Payment $payment
     * @return bool
     */
    public function verify(Payment $payment) : bool;
}
