<?php

namespace App\Services\Payment\Contracts;

use App\Entities\Payment;

interface PaymentServiceInterface
{
    public function goToGateWay(Payment $payment) : void;

    public function verifyPayment(Payment $payment) : bool;
}
