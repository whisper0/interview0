<?php

namespace App\Services\Payment;

use App\Drivers\Payment\Contracts\PaymentDriverInterface;
use App\Entities\Payment;
use App\Services\Payment\Contracts\PaymentServiceInterface;

class PaymentService implements PaymentServiceInterface
{
    /** php 8 implementation */
//    public function __construct(private PaymentDriverInterface $paymentDriver)
//    {
//    }

    /** php 7 implementation (for some reason im using 7 right now and don't have time to change it) */
    public PaymentDriverInterface $paymentDriver;

    public function __construct(PaymentDriverInterface $paymentDriver)
    {
        $this->paymentDriver = $paymentDriver;
    }

    public function goToGateWay(Payment $payment): void
    {
        //some logics...
        $this->paymentDriver->pay($payment);
    }

    public function verifyPayment(Payment $payment): bool
    {
        //some logics...

        return $this->paymentDriver->verify($payment);
    }
}
