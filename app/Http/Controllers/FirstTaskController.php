<?php

namespace App\Http\Controllers;

use App\Helpers\Formatter;

class FirstTaskController extends Controller
{
    public function firstTask()
    {
        return  Formatter::humanReadableNumber('100000.111222333', null, 'fa');
    }

}
