<?php

namespace App\Http\Controllers;

use App\Entities\Payment;
use App\Services\Payment\Contracts\PaymentServiceInterface;

class SecondTaskController extends Controller
{

    public function goToGateway(PaymentServiceInterface $paymentService)
    {
        //TODO: get and validate response

        $payment = new Payment(1,1,10000);

        $paymentService->goToGateWay($payment);

        //TODO: transform data to response
    }

}
