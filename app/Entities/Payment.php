<?php

namespace App\Entities;

class Payment
{
    private int $userId;
    private int $productId;
    private string $price;
    //..........more fields

    public function __construct($userId, $productId, $amount)
    {
        $this->userId = $userId;
        $this->productId = $productId;
        $this->price = $amount;
    }

    public function setUserId(int $userId) : self
    {
        $this->userId = $userId;
        return $this;
    }

    public function getUserId() : int
    {
        return $this->userId;
    }

    //TODO: implement setters and getters
}
