<?php

namespace App\Helpers;

class Formatter
{
    public static function humanReadableNumber($number, $decimalPlaces = null, $language = 'en') : string
    {
        // Check if the number is given as a string and contains 'e' or 'E' for scientific notation
        if (is_string($number) && preg_match('/[eE]/', $number)) {
            $newNum = sprintf('%F', $number); // Convert the scientific notation to a regular floating-point number

            if ($newNum == 0.000000) {
                $number = number_format($number, 20, '.', '');
            } else {
                $number = $newNum;
            }
        }

        if($decimalPlaces && $decimalPlaces > self::getDecimalCount($number)) {
            return $number;
        }

        // Set the locale for number formatting
        if ($language === 'fa') {
            $formatter = numfmt_create('fa_IR', \NumberFormatter::DECIMAL_SEPARATOR_SYMBOL);
        } else {
            $formatter = numfmt_create('en_US', \NumberFormatter::TYPE_DEFAULT);
        }

        // Format the number with the given number of decimal places
        if ($decimalPlaces !== null) {
            $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, $decimalPlaces);
        }

        // Format the number to a human-readable format
        $res = numfmt_format($formatter, $number);
        if($number < 0 && $language == 'fa') {
            $res = str_replace('‎−', '-', $res);
        }

        return $res;
    }


    public static function getDecimalCount($number) {
        // Convert the number to a string
        $numberString = strval($number);

        // Check if the number contains a decimal point
        if (strpos($numberString, '.') !== false) {
            // Get the substring after the decimal point
            $decimalPart = substr($numberString, strpos($numberString, '.') + 1);

            // Count the number of characters in the decimal part
            return strlen($decimalPart);
        } else {
            // If there is no decimal point, return 0
            return 0;
        }
    }

}
